json-smart (2.2-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rebuild for unstable.  (Closes: #1039985)

 -- Andreas Beckmann <anbe@debian.org>  Tue, 09 Apr 2024 10:38:03 +0200

json-smart (2.2-2+deb12u1) bookworm; urgency=medium

  * Non-maintainer upload.
  * Rebuild for bookworm.  (Closes: #1039985)

 -- Andreas Beckmann <anbe@debian.org>  Tue, 09 Apr 2024 10:01:36 +0200

json-smart (2.2-2+deb11u1) bullseye; urgency=medium

  * Non-maintainer upload.
  * Update Vcs-* URLs to point to salsa.debian.org.
  * Rebuild for bullseye.  (Closes: #1039985)

 -- Andreas Beckmann <anbe@debian.org>  Tue, 09 Apr 2024 09:36:58 +0200

json-smart (2.2-2+deb10u1) buster-security; urgency=high

  * Non-maintainer upload by the LTS team.
  * CVE-2023-1370: stack overflow due to excessive recursion
    When reaching a ‘[‘ or ‘{‘ character in the JSON input, the code
    parses an array or an object respectively. It was discovered that the
    code does not have any limit to the nesting of such arrays or
    objects. Since the parsing of nested arrays and objects is done
    recursively, nesting too many of them can cause a stack exhaustion
    (stack overflow) and crash the software. (Closes: #1033474)
  * CVE-2021-31684: Fix indexOf
    A vulnerability was discovered in the indexOf function of
    JSONParserByteArray in JSON Smart versions 1.3 and 2.4
    which causes a denial of service (DOS)
    via a crafted web request.

 -- Bastien Roucariès <rouca@debian.org>  Wed, 29 Mar 2023 22:21:33 +0000

json-smart (2.2-2) unstable; urgency=medium

  * Team upload.
  * Add maven-bundle-plugin-failok.patch and work around a FTBFS
    caused by a bug in maven-bundle-plugin. See #868913 for more information.
    (Closes: #868603)
  * Switch to compat level 10.
  * Declare compliance with Debian Policy 4.1.1.
  * Use https for format field.

 -- Markus Koschany <apo@debian.org>  Mon, 16 Oct 2017 15:52:50 +0200

json-smart (2.2-1) unstable; urgency=medium

  * New upstream release
    - Build the new accessors-smart module
    - Ignore the timezone dependent tests in TestDateConvert
  * Standards-Version updated to 3.9.8

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 03 Aug 2016 10:06:43 +0200

json-smart (1.2-1) unstable; urgency=medium

  * Initial release (Closes: #819635)

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 31 Mar 2016 13:21:52 +0200
